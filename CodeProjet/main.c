//#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <setjmp.h>
#include <math.h>
#include <unistd.h>

#define MAX 30
#define FEXT ".txt" 

typedef char MY_TYPE;

typedef struct {
   char heure[3]; //06
   char minute[3]; //40
   char secondes[3]; //36
   char centiemes[5];//.289
} Temps;

typedef struct { 
   char latitude[11]; //4836.5375
   char longitude[12]; //00740.9373
   char orien1[3]; //N ou S
   char orien2[3]; //W ou E

} Trame;

typedef enum {OK,PAS_GPGGA,CHAMP_INVALIDE,PTR_NULL,INVALID_ARGUMENT} Exception;

//Recupere une sous-chaine d'une chaine ch de l'indice debut+orign jusqu'à n
char * retrievesubstr(char *ch,const char *orign,int debut, int n){
   strncpy(ch,(orign+debut),n);
   ch[n]='\0';//Neutralisation de la fin de chaine
   return ch;
}

//Indique si la chaine ch passée en paramètre est une trame GPGGA
//Retourne 1 si ch est une trame GPGGA
//Lève PAS_GPGGA si ch n'est pas une trame GPGGA
int estGPGGA(char ch[],jmp_buf ptRep){
   if(strstr(ch,"$GPGGA")!=NULL && strlen(ch)>15){//Si il y le motif "GPGGA" dans la trame et plus de 15 chars, OK
      return 1;
   } else {
    longjmp(ptRep, PAS_GPGGA);
    return 0;
  }
}

//Convertit une chaine de caractères ch en float p, et transforme p en degré
//minute et secondes
//Retourne un pointeur char
//Lève PTR_NULL si ch==NULL
char * convertirLonLat(char * ch, jmp_buf ptRep){
   if(ch==NULL){
      longjmp(ptRep,PTR_NULL);
   }
   double p = atof(ch); //Transformation de la chaine de caractères en double
   double minutet; //Variable de transition
   int degre;
   int minute;
   double secon;
   p=p/100;
   degre = ((int)floor(p));
   minutet = (p-degre)*60;
   minute=(int)floor(minutet);
   secon =(minutet-minute)*60;
   char * str = calloc(14,sizeof(char));
   sprintf(str,"%d°%d'%.*f''",degre,minute,2,secon);
   return str;
}

//Formate une chaine str construite à partir des champs d'une structure
//Temps, au format XXhYYmZZs
//Lève PTR_NULL si tem==NULL
char * formaterHeure(Temps * tem, jmp_buf ptRep){
   if(tem==NULL){
      longjmp(ptRep,PTR_NULL);
   }
   char * str = calloc(14,sizeof(char));
   sprintf(str,"%sh%sm%ss",tem->heure,tem->minute,tem->secondes);
   return str;

}

//Affiche le temps de réception au format XXhYYmZZs
//Lève PTR_NULL si tem==NULL
//Utilise la fonction formaterHeure
void affichageHeure(Temps * tem, jmp_buf ptRep){
   if(tem==NULL){
      longjmp(ptRep,PTR_NULL);
   }
   char * ch = formaterHeure(tem,ptRep); //Formate l'heure avant de l'afficher
   printf("Horaire de réception : %s\n\n",ch);
}

//Affiche la latitude et la longitude au format XX°YY'ZZ''
//Lève PTR_NULL si trm==NULL
//Utilise la fonction convertirLonLat
void affichageLonLat(Trame * trm, jmp_buf ptRep){
   printf("Latitude : %s,%s\n",convertirLonLat(trm->latitude,ptRep),trm->orien1);
   printf("Longitude : %s,%s\n",convertirLonLat(trm->longitude,ptRep),trm->orien2);
}

//Affecte aux structures Temps temps et Trame trame les informations
//voulues en fonction de la position du séparateur ',' dans la chaine trm
//Lève PTR_NULL si temps==NULL ou trame ==NULL
//Utilise la fonction retrievesubstr
void recupererInfo(char * trm,Trame * trame, Temps * temps, jmp_buf ptRep){
   char liste[20][20]; //Tableau à deux dimensions de caractères
   char * firstOcc;
   char test[55]="";
   char * testP= test;
   char * ptrm = trm;
   char virgule[]=",";
   firstOcc = strstr(trm,virgule);//Affecte à firstOcc la position de la
   //première occurence de ','
   for(int i=0;i<5/*5*/;i++){ //Il faut parcourir cinq champs pour obtenir toutes
      //les informations
      ptrm = firstOcc+strlen(virgule); //On incrémente le pointeur sur trm de la
      //valeur du séparateur ','
      testP = firstOcc; //On garde la valeur de firstOcc
      firstOcc = strstr(ptrm,virgule); // On met à jour firstOcc
      retrievesubstr(test,trm,testP-trm+1,firstOcc-testP-1); //On récupère le
      //string dans trm,à partir de testP-trm+1 pour firstOcc-testP-1 et on
      //affecte le résultat à test
      for(int j=0;j<strlen(test);j++){//On attribue à liste[i] la valeur de test
	 liste[i][j]=test[j];         //pour j allant de 0 jusqu'à strlen(test)
      }
   }
   liste[0][10]='\0'; //Fin de chaine

   strncpy(trame->latitude,liste[1],9); //La latitude comporte 9 caractères
   strncpy(trame->longitude,liste[3],10); //La longitude comporte 10 caractères
   strncpy(trame->orien1,liste[2],1); //Un seul charactère pour l'orientation azimut
   strncpy(trame->orien2,liste[4],1); //Un seul charactère pour l'orientation pitch

   for(int i=0;i<2;i++){
      temps->heure[i]=liste[0][i]; //Affection de l'heure
   }
   temps->heure[2]='\0'; //Neutralisation de la chaine heure
   for(int i=2;i<4;i++){
      temps->minute[i-2]=liste[0][i]; //Affection des minutes
   }
   temps->minute[2]='\0'; //Neutralisation de la chaine minutes
   for(int i=4;i<6;i++){
      temps->secondes[i-4]=liste[0][i]; //Affectation des secondes
   }
   temps->secondes[2]='\0'; //Neutralisation de la chaine secondes
   for(int i=7;i<10;i++){
      temps->centiemes[i-7]=liste[0][i]; //Affectation des centiemes
   }
}

void voidTextFile(){
	FILE *fic = fopen("ResultatTrames","w"); //Ouvre le fichier en mode écriture
	fprintf(fic,"TRAMES :\n"); //Ecrase les données précedentes
	fclose(fic);
}

//Ecrit dans un fichier les informations à partir de deux structures Temps et Trame
//Lève PTR_NULL si fichier inexistant
   void rangerDansFile(Trame * trm, Temps * tem,jmp_buf ptRep,int * nb){
      FILE *fic = fopen("ResultatTrames","a+"); //Mode a+, append en fin de fichier
   char * chlat=convertirLonLat(trm->latitude,ptRep); //Conversion de la latitude
   char * chlon=convertirLonLat(trm->longitude,ptRep); //Conversion de la longitude
   char * chtemps = formaterHeure(tem,ptRep); //Formatage heure
   char cnb = *nb;
   char * pcnb = &cnb;

   if(fic ==NULL){
      printf("Erreur a l'ouverture du fichier");
      longjmp(ptRep,PTR_NULL);
   }
   else{
   fputs("----------------\n",fic);
   fputs("Trame n",fic);
   fputc(cnb,fic);
   fputs("\n",fic);
   fputs("----------------\n",fic);

   fputs("Latitude : ",fic);
   fputs(chlat, fic);
   fputs(",",fic);
   fputs(trm->orien1,fic);
   fputs("\n",fic);

   fputs("Longitude : ",fic);
   fputs(chlon, fic);
   fputs(",",fic);
   fputs(trm->orien2,fic);
   fputs("\n",fic);

   fputs("Temps de réception :",fic);
   fputs(chtemps, fic);
   fputs("\n\n",fic);
      }
   fclose(fic);
   }

void rangerDansFileErrone(jmp_buf ptRep){

	FILE *fic = fopen("ResultatTrames","a+");
	if(fic==NULL){
		printf("Erreur a l'ouverture du fichier");
		longjmp(ptRep,PTR_NULL);
	} else{

		fputs("Trame erronée\n",fic);
	}
}

//Lit une ou plusieurs trames depuis un fichier texte
//Permet également de connaitre le nombre de trames
//Lève PTR_NULL si fichier inexistant ou innacessible
void lireFichier(jmp_buf ptRep,char * ch){
   FILE *fic;
   int nb=1;
   int * pnb = &nb;
   voidTextFile(); //Vide le fichier cible
   Exception erreur;
   Temps temps;
   Temps * pTemps=&temps;
   Trame trame;
   Trame * pTrame=&trame;
   Trame eTrame={0}; //struct Trame vide utilisée dans la boucle
   Temps eTemps={0}; //struct Temps vide utilisée dans la boucle
   char trm[50];

   fic = fopen(ch,"r");
   if(fic==NULL){
      printf("Erreur à l'ouverture du fichier");
      longjmp(ptRep,PTR_NULL);
   } else{
      printf("La lecture des trames va commencer\n\n");
      char* ligne = NULL;
      size_t taille=0;
      erreur=setjmp(ptRep);
      getline(&ligne,&taille,fic);
      strncpy(trm,ligne,45);
      while(!feof(fic)){
	//sleep(2);
	printf("------------------------------------\n");
	printf("Trame n° %d\n",nb);
	printf("------------------------------------\n");
	nb++;
	strncpy(trm,ligne,45);
		if(estGPGGA(trm,ptRep)==1){
		 	printf("Trame traitée : %s",ligne);
	 		recupererInfo(trm,pTrame,pTemps,ptRep);
	 		affichageLonLat(pTrame,ptRep);
	 		affichageHeure(pTemps,ptRep);
	 		rangerDansFile(pTrame,pTemps,ptRep,pnb);
		}else{
			printf("Trame erronée");
			rangerDansFileErrone(ptRep);
		}
	trame=eTrame;
	temps=eTemps;
	pTrame=&trame;
	pTemps=&temps;
	getline(&ligne,&taille,fic);
      }
      free(ligne);
      fclose(fic);
      }
   }

/*char * recupererIdFile() {
	printf("Rentrez un nom de fichier .txt :");
	char fname[128];
	scanf("%123s",fname);
	strcat(fname,".txt");
	char * filename = (char *) malloc(MAX*sizeof(char));
	assert(filename!=NULL);
	strcpy(filename,fname);
	return filename;
}
*/


int main() {
   jmp_buf ptRep;
   Exception erreur;
   erreur=setjmp(ptRep);
   lireFichier(ptRep,"proto.txt");
}


