//Affiche le temps de reception au format XXhYYmZZs
//Leve PTR_NULL si tem==NULL
//Utilise la fonction formaterHeure
void affichageHeure(Temps * tem, jmp_buf ptRep);



//Affiche la latitude et la longitude au format XX°YY'ZZ''
//Leve PTR_NULL si trm==NULL
//Utilise la fonction convertirLonLat
void affichageLonLat(Trame * trm, jmp_buf ptRep);
