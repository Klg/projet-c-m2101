//Affiche le temps de reception au format XXhYYmZZs
//Leve PTR_NULL si tem==NULL
//Utilise la fonction formaterHeure
void affichageHeure(Temps * tem, jmp_buf ptRep){
   if(tem==NULL){
      longjmp(ptRep,PTR_NULL);
   }
   char * ch = formaterHeure(tem,ptRep); //Formate l'heure avant de l'afficher
   printf("Heure : %s\n\n",ch);
}

//Affiche la latitude et la longitude au format XX°YY'ZZ''
//Leve PTR_NULL si trm==NULL
//Utilise la fonction convertirLonLat
void affichageLonLat(Trame * trm, jmp_buf ptRep){
   printf("Latitude : %s,%s\n",convertirLonLat(trm->latitude,ptRep),trm->orien1);
   printf("Longitude : %s,%s\n",convertirLonLat(trm->longitude,ptRep),trm->orien2);
}
