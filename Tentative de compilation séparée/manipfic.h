void voidTextFile();


//Ecrit dans un fichier les informations a partir de deux structures Temps et Trame
//Leve PTR_NULL si fichier inexistant
void rangerDansFile(Trame * trm, Temps * tem,jmp_buf ptRep,int * nb);
 


//Ecrit dans le fichier quand une trame est invalide
//Leve PTR_NULL si fic==NULL
void rangerDansFileErrone(jmp_buf ptRep);



//Lit une ou plusieurs trames depuis un fichier texte
//Contient le traitement complet d'une trame
//Leve PTR_NULL si fichier inexistant ou innacessible
void lireFichier(jmp_buf ptRep,char * ch);


//Permet a l'utilisateur de rentrer un nom de fichier
/*char * recupererIdFile();
