void voidTextFile(){
   FILE *fic = fopen("ResultatTrames","w"); //Ouvre le fichier en mode ecriture
   fprintf(fic,"TRAMES :\n"); //Ecrase les donnees precedentes
   fclose(fic);
}

//Ecrit dans un fichier les informations a partir de deux structures Temps et Trame
//Leve PTR_NULL si fichier inexistant
void rangerDansFile(Trame * trm, Temps * tem,jmp_buf ptRep,int * nb){
   FILE *fic = fopen("ResultatTrames","a+"); //Mode a+, append en fin de fichier
   char * chlat=convertirLonLat(trm->latitude,ptRep); //Conversion de la latitude
   char * chlon=convertirLonLat(trm->longitude,ptRep); //Conversion de la longitude
   char * chtemps = formaterHeure(tem,ptRep); //Formatage heure
   char cnb = *nb;
   char * pcnb = &cnb;

   if(fic ==NULL){
      printf("Erreur a l'ouverture du fichier");
      longjmp(ptRep,PTR_NULL);
   }
   else{
      fputs("----------------\n",fic);
      fputs("Trame n",fic);
      fputc(cnb,fic);
      fputs("\n",fic);
      fputs("----------------\n",fic);

      fputs("Latitude : ",fic);
      fputs(chlat, fic);
      fputs(",",fic);
      fputs(trm->orien1,fic);
      fputs("\n",fic);

      fputs("Longitude : ",fic);
      fputs(chlon, fic);
      fputs(",",fic);
      fputs(trm->orien2,fic);
      fputs("\n",fic);

      //printf("OUI");
      fputs("Chronologie de reception :",fic);
      fputs(chtemps, fic);
      fputs("\n\n",fic);
   }
   fclose(fic);
}

//Ecrit dans le fichier quand une trame est invalide
//Leve PTR_NULL si fic==NULL
void rangerDansFileErrone(jmp_buf ptRep){

   FILE *fic = fopen("ResultatTrames","a+");
   if(fic==NULL){
      printf("Erreur a l'ouverture du fichier");
      longjmp(ptRep,PTR_NULL);
   } else{

      fputs("Trame erronée\n",fic);
   }
}

//Lit une ou plusieurs trames depuis un fichier texte
//Contient le traitement complet d'une trame
//Leve PTR_NULL si fichier inexistant ou innacessible
void lireFichier(jmp_buf ptRep,char * ch){
   FILE *fic;
   int nb=1;
   int * pnb = &nb;
   voidTextFile(); //Vide le fichier cible
   Exception erreur;
   Temps temps;
   Temps * pTemps=&temps;
   Trame trame;
   Trame * pTrame=&trame;
   Trame eTrame={0}; //struct Trame vide utilisable dans la boucle
   Temps eTemps={0}; //struct Temps vide utilisable dans la boucle
   char trm[50];

   fic = fopen(ch,"r");
   if(fic==NULL){
      printf("Erreur à l'ouverture du fichier");
      longjmp(ptRep,PTR_NULL);
   } else{
      char* ligne = NULL;
      size_t taille=0;
      erreur=setjmp(ptRep);
      getline(&ligne,&taille,fic);
      strncpy(trm,ligne,45);
      while(!feof(fic)){
	 printf("------------------------------------\n");
	 printf("Trame n° %d\n",nb);
	 printf("------------------------------------\n");
	 nb++;
	 strncpy(trm,ligne,45);
	 if(estGPGGA(trm,ptRep)==1){
	    printf("%s",ligne);
	    recupererInfo(trm,pTrame,pTemps,ptRep);
	    affichageLonLat(pTrame,ptRep);
	    affichageHeure(pTemps,ptRep);
	    rangerDansFile(pTrame,pTemps,ptRep,pnb);
	 }else{
	    printf("Trame erronée");
	    rangerDansFileErrone(ptRep);
	 }
	 trame=eTrame;
	 temps=eTemps;
	 pTrame=&trame;
	 pTemps=&temps;
	 getline(&ligne,&taille,fic);
      }
      free(ligne);
      fclose(fic);
   }
}

//Permet a l'utilisateur de rentrer un nom de fichier
/*char * recupererIdFile() {
	printf("Rentrez un nom de fichier .txt :");
	char fname[128];
	scanf("%123s",fname);
	strcat(fname,".txt");
	char * filename = (char *) malloc(MAX*sizeof(char));
	assert(filename!=NULL);
	strcpy(filename,fname);
	return filename;
  }
 */
