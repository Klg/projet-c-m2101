//Affecte aux structures Temps temps et Trame trame les informations
//voulues en fonction de la position du separateur ',' dans la chaine trm
//Leve PTR_NULL si temps==NULL ou trame ==NULL
//Utilise la fonction retrievesubstr
void recupererInfo(char * trm,Trame * trame, Temps * temps, jmp_buf ptRep){
   char liste[20][20]; //Tableau a deux dimensions de caracteres
   char * firstOcc;
   char test[55]="";
   char * testP= test;
   char * ptrm = trm;
   char virgule[]=",";
   firstOcc = strstr(trm,virgule);//Affecte a firstOcc la position de la
   //premiere occurence de ','
   for(int i=0;i<5/*5*/;i++){ //Il faut parcourir cinq champs pour obtenir toutes
      //les informations
      ptrm = firstOcc+strlen(virgule); //On incremente le pointeur sur trm de la
      //valeur du separateur ','
      testP = firstOcc; //On garde la valeur de firstOcc
      firstOcc = strstr(ptrm,virgule); // On met a jour firstOcc
      retrievesubstr(test,trm,testP-trm+1,firstOcc-testP-1); //On recupere le
      //string dans trm,a partir de testP-trm+1 pour firstOcc-testP-1 et on
      //affecte le resultat a test
      for(int j=0;j<strlen(test);j++){//On attribue�a liste[i] la valeur de test
	 liste[i][j]=test[j];         //pour j allant de 0 jusqu'a strlen(test)
      }
   }
   liste[0][10]='\0'; //Fin de chaine

   strncpy(trame->latitude,liste[1],9); //La latitude comporte 9 caracteres
   strncpy(trame->longitude,liste[3],10); //La longitude comporte 10 caracteres
   strncpy(trame->orien1,liste[2],1); //Un seul charactere pour l'orientation azimut
   strncpy(trame->orien2,liste[4],1); //Un seul charactere pour l'orientation pitch

   for(int i=0;i<2;i++){
      temps->heure[i]=liste[0][i]; //Affection de l'heure
   }
   temps->heure[2]='\0'; //Neutralisation de la chaine heure
   for(int i=2;i<4;i++){
      temps->minute[i-2]=liste[0][i]; //Affection des minutes
   }
   temps->minute[2]='\0'; //Neutralisation de la chaine minutes
   for(int i=4;i<6;i++){
      temps->secondes[i-4]=liste[0][i]; //Affectation des secondes
   }
   temps->secondes[2]='\0'; //Neutralisation de la chaine secondes
   for(int i=7;i<10;i++){
      temps->centiemes[i-7]=liste[0][i]; //Affectation des centiemes
   }
}

