# Projet C M2101

![cons](Capture.PNG)

Projet de M2101 des étudiants Samuel De Amoedo Amorim et Kilyan Le Gallic portant
sur le traitement de trames GPS NMEA format GPGGA en utilisant le langage C     
Nous avons exploité les informations concernant la norme GPS NMEA0183
* [https://framagit.org/Kilyan-Le_Gallic/projet-c-m2101.git](https://framagit.org/Kilyan-Le_Gallic/projet-c-m2101.git)

# *INFORMATIONS UTILES*

Les fichiers utilés par le programme doivent se trouver dans le même repertoire 
que le fichier ***main.c***
Le fichier où se trouve le résultat du programme se créera automatiquement sous 
le nom de ***ResultatTrames***
Le fichier où se trouve les trames à traiter doit être nommé ***proto.txt***

# Ressources et documents utiles :

* [https://en.wikipedia.org/wiki/NMEA_0183](https://en.wikipedia.org/wiki/NMEA_0183)
* [https://www.gpsinformation.org/dale/nmea.htm](https://www.gpsinformation.org/dale/nmea.htm)

# Documentation :

```c
typedef struct {
   char heure[3]; //06
   char minute[3]; //40
   char secondes[3]; //36
   char centiemes[5];//.289
} Temps;

typedef struct { //Nous supposons que l'orientation est Lat,N,Lon,E
   char latitude[11]; //4836.5375
   char longitude[12]; //00740.9373
   char orien1[3]; //N ou S
   char orien2[3]; //W ou E
} Trame;
```
# Exceptions :
```c
typedef enum {OK,PAS_GPGGA,CHAMP_INVALIDE,PTR_NULL} Exception;
```

# Méthodes :

|Sommaire des fonctions                                                                                    |
| ------                                                                                                   |
|   [RetrieveSubStr](#retrievesubstr) - Recupere une sous-chaîne                                   |
|   [EstGPGGA](#estgpgga) - Indique si la trame est du format GPGGA                                         |
|   [ConvertirLonLat](#convertirlonlat) - Convertit au format sexagésimal                     |
|   [FormaterHeure](#formaterheure) - Formate l'heure au format XXhYYmZZs                    |
|   [AffichageHeure](#affichageheure) - Afficher l'heure formatée            |
|   [AffichageLonLat](#affichagelonlat) - Affiche la latitude et la longitude                                        |
|   [recupererInfo](#recupererinfo) - Affecte aux structures les informations de la trame                         |
|   [VoidTextFile](#voidtextfile) - Vide le fichier cible avant d'écrire                                             |
|   [RangerDansFile](#rangerdansfile) - Ecrit les informations dans le fichier                                     |
|   [RangerDansFileErrone](#rangerdansfileerrone) - Ecrit "TRAME ERRONE" dans le fichier                                                      |
|   [LireFichier](#lirefichier) - Lit et traite les trames dans un fichier        |


### RetrieveSubStr
```c
char * retrievesubstr(char *ch,const char *orign,int debut, int n);
```
Recupere une sous-chaine d'une chaine ch de l'indice debut+orign jusqu'à n
>Parameters :
>
>   * char * ch- Chaine cible
>    
>   * char * orign - Chaine source
>
>   * int * debut - Indice de début de copie
>
>   * int * n - Nombre de caractères à copier

>Returns:
>
>  * char * - Retourne la chaine voulue





### EstGPGGA
```c
int estGGPA(char ch[],jmp_buf ptRep);
```
Indique si la chaine ch passée en paramètre est une trame GPGGA
Retourne 1 si ch est une trame GPGGA
Lève PAS_GPGGA si ch n'est pas une trame GPGGA
>Parameters :
>   * char ch[] - Chaine a évaluer
>
>   * jmp_buf ptRep - Point de reprise

>  Returns :
>
>   * int - Etat de la condition sur la trame





### ConvertirLonLat
```c
char * convertirLonLat(char * ch, jmp_buf ptRep);
```
Convertit une chaine de caractères ch en float p, et transforme p en degré
minute et secondes
Retourne un pointeur char
Lève PTR_NULL si ch==NULL
>Parameters :
>   * char * ch - Chaine a convertir
>
>   * jmp_buf ptRep - Point de reprise

>  Returns :
>
>   * char * - Information traitée





### FormaterHeure
```c
char * formaterHeure(Temps * tem, jmp_buf ptRep);
```
Formate une chaine str construite à partir des champs d'une structure
Temps, au format XXhYYmZZs
Lève PTR_NULL si tem==NULL

>Parameters :
>   * Temps * tem - Structure Temps à traiter
>
>   * jmp_buf ptRep - Point de reprise

>  Returns :
>
>   * char * - Heure formatée





### AffichageHeure
```c
void affichageHeure(Temps * tem, jmp_buf ptRep);
```
Affiche le temps de réception au format XXhYYmZZs
Lève PTR_NULL si tem==NULL
Utilise la fonction formaterHeure
>Parameters :
>   * Temps * tem - Structure Temps à afficher
>
>   * jmp_buf ptRep - Point de reprise





### AffichageLonLat
```c
void affichageLonLat(Trame * trm, jmp_buf ptRep);
```
Affiche la latitude et la longitude au format XX°YY'ZZ''
Lève PTR_NULL si trm==NULL
Utilise la fonction convertirLonLat

>Parameters :
>   * Trame * trm - Structure Trame à afficher
>
>   * jmp_buf ptRep - Point de reprise





### RecupererInfo
```c
void recupererInfo(char * trm,Trame * trame, Temps * temps, jmp_buf ptRep);
```
Affecte aux structures Temps temps et Trame trame les informations
voulues en fonction de la position du séparateur ',' dans la chaine trm
Lève PTR_NULL si temps==NULL ou trame ==NULL
Utilise la fonction retrievesubstr
>Parameters :
>   * char * trm - Trame complète
>
>   * Trame * trame - Structure Trame à traiter
>
>   * Temps * temps - Structure Temps à traiter
>
>   * jmp_buf ptRep - Point de reprise


### VoidTextFile
```c
void voidTextFile();
```
Efface le texte du fichier où seront écrits les données extraites des trames
Lève PTR_NULL si le fichier est innacesible





### RangerDansFile
```c
void rangerDansFile(Trame * trm, Temps * tem,jmp_buf ptRep);
```
Ecrit dans un fichier les informations à partir de deux structures Temps et Trame
Lève PTR_NULL si fichier innacesible
>Parameters :
>   * Trame * trm - Structure Trame à écrire dans le fichier
>
>   * Temps * tem - Structure Temps à écrire dans le fichier
>
>   * jmp_buf ptRep - Point de reprise






### RangerDansFileErrone
```c
void rangerDansFileErrone(jmp_buf ptRep);
```
Ecrit dans le fichier quand une trame est invalide
Leve PTR_NULL si fic==NULL






### LireFichier
```c
void lireFichier(jmp_buf ptRep,char * ch);
```
Recupère les trames brutes depuis un fichier texte et utilise les méthodes de
traitement des trames dans la boucle de lecture des lignes
Fait appel aux méthodes précedentes
Lève PTR_NULL si fichier innacesible ou inexistant
>Parameters :
>   * jmp_buf ptRep - Point de reprise
>
>   * char * ch - Nom du fichier à lire


**NON UTILISE**
```c
char * recupererIdFile();
```
Permet a l'utilisateur de rentrer un nom de fichier
>  Returns :
>
>   * char * - Nom du fichier désiré


[^1]: Réalisé par Kilyan Le Gallic et Samuel Amoedo Amorim